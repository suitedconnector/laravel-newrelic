<?php

namespace App\Library\Utility;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Log;
use Psr\Http\Message\ResponseInterface;

/**
 * Class NewRelic
 *
 * @package App\Library\Utility
 */
class NewRelic
{
	/**
	 *  Key for NewRelic api
	 */
	const NEWRELIC_KEY = 'aLLGkB6xtbcmASfQ33FP0Fy8Fe-qCv3v';
	/**
	 *  URL for NewRelic api
	 */
	const NEWRELIC_URL = 'https://insights-collector.newrelic.com/v1/accounts/680362/events';
	const CONNECTION_TIMEOUT = 30;
	
	/**
	 * @param string $eventType
	 * @param array $details
	 */
	static function fireEvent(string $eventType, array $details)
	{
		$prefix = env('NEWRELIC_PREFIX', '');
		$details['eventType'] = $prefix . $eventType;
		$httpClient = new HttpClient(
			[
				'verify' => FALSE,
				'version' => 1.0,
				'expect' => FALSE,
				'connect_timeout' => self::CONNECTION_TIMEOUT
			]);
			$promise = $httpClient->postAsync(self::NEWRELIC_URL, [
				'headers' => [
					'X-Insert-Key' => self::NEWRELIC_KEY
				],
				'json' => $details
			]);
			$promise->then(
				function (ResponseInterface $res) {
					// no op
				},
				function (RequestException $e) use ($details) {
					$code = "???";
					if ($e->hasResponse()) {
						$errorResponse = $e->getResponse();
						$code = $errorResponse->getStatusCode();
					}
					$message = $e->getMessage();
					Log::error("NewRelic event posting failure: {$code} {$message}", $details);
				}
			);
			// unfortunately, async calls don't work, so force resolution
		$promise->wait();
	}
}
