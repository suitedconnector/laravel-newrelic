# SuitedConnector Laravel-NewRelic

This repository serves as a Composer package containing a helper library for reporting to NewRelic.

## Including in an existing project

Add the following in your `composer.json` file

```
    "repositories": [
        {
            "type":"git",
            "url": "https://bitbucket.org/suitedconnector/laravel-newrelic.git"
        }
    ],
    "require": {
        "suitedconnector/laravel-newrelic": "dev-master"
    },
```

The helper library is added to the `App\Library\Utility` namespace, and will be usable with:

```
use App\Library\Utility\NewRelic;
```
There is a single static method.
NewRelic::fireEvent(string $eventType, array $details);
